'use strict';

// write front-end SAFE variables to config file
const fs = require('fs');
const compileTemplate = require('lodash/template');
const loadDotEnv = require('cmc-load-dot-env');

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

module.exports = function writeConfig(options) {
    fs.readFile(
        `${options.outputPath}/config.tmpl`,
        'utf8',
        (readTemplateError, template) => {
            if (readTemplateError) {
                // eslint-disable-next-line no-console
                console.log('error reading template file', readTemplateError);
            } else {
                const compiled = compileTemplate(template, { variable: 'data' });
                const result = compiled(process.env);

                fs.writeFile(
                    `${options.outputPath}/config.js`,
                    result,
                    'utf8',
                    (writeConfigError) => {
                        if (writeConfigError) {
                            // eslint-disable-next-line no-console
                            console.log(
                                'error writing config file',
                                writeConfigError,
                            );
                        } else {
                            // eslint-disable-next-line no-console
                            console.log(
                                `Generated new config.js file in folder: ${
                                    options.outputPath
                                }`,
                            );
                        }
                    },
                );
            }
        },
    );
};
